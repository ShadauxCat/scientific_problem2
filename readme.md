This is my answer to problem 2 of the scientific games programming challenge.

This answer requires some setup before it can be executed.

1. You must ensure that the mcrypt mod is enabled (`sudo php5enmod mcrypt`)
2. You must have a mysql server running, and you must enter admin credentials into `settings.php`
3. Open your browser and load up $hostname/install.php to initialize the database

The install script creates a database with a single test user (user id 1)

Once you do that, use your preferred manner of interacting with a REST interface to submit a PUT request with a JSON payload in this form:

```
{
	"hash":"<hash>",
	"coinsWon":10,
	"coinsBet":5,
	"playerID":1
}
```

Since there's no real client to actually compute a hash, a request with an invalid hash will include the valid hash in the error code, so submit a request with an invalid hash, and use the response to make the request valid.

As a note, I wasn't sure what the hash was supposed to be a hash of. In my experience, hashes are usually hashes of passwords, especially when salts are involved, so I added a password field to the database that wasn't specified by the problem specification. I hope that isn't outside the bounds of the project.