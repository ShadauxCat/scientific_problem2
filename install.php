<?php
	require_once('settings.php');

	// This script initializes the database.

	$mysql = null;

	// Print an error message, set a response code, and exit
	function error($msg, $code)
	{
		if($mysql)
		{
			$mysql->close();
		}
		echo($msg);
		http_response_code($code);
		die();
	}

	// Connect to the 
	$mysql_settings = $settings['mysql'];
	$servername = $mysql_settings['host'];
	$dbname = $mysql_settings['databaseName'];
	try
	{
		$mysql = new PDO("mysql:host=$servername", $mysql_settings['user'], $mysql_settings['password'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	}
	catch(PDOException $e)
	{
		error("Could not connect to mysql database.", 500);
	}

	echo("Connected to mysql instance... Installing...<br>");

	// Create the database if it doesn't exist...
	$dbName = $mysql_settings['databaseName'];
	$sql = "CREATE DATABASE IF NOT EXISTS $dbName";
	try
	{
		$mysql->query($sql);
	}
	catch(PDOException $e)
	{
		error("Error creating database: " . $e->getMessage(), 500);
	}

	echo(".. Database '$dbName' created successfully.<br>");

	// Create the players table if it doesn't exist...
	$sql = "CREATE TABLE IF NOT EXISTS $dbname.Players (
			playerID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(30) NOT NULL UNIQUE,
			credits INT,
			lifetimeSpins INT UNSIGNED,
			salt VARCHAR(64) NOT NULL,
			hashedPassword VARCHAR(60) NOT NULL
		)";

	try
	{
		$mysql->query($sql);
	}
	catch(PDOException $e)
	{
		error("Error creating table: " . $e->getMessage(), 500);
	}

	echo(".. Table 'Players' created successfully.<br>");

	// And finally insert a single test player into the database
	$testSalt = $mysql->quote(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
	$startingCredits = $settings['game']['startingCredits'];
	$testHash = password_hash("password123SECURE!", PASSWORD_BCRYPT, array('salt' => $testSalt));

	$sql = "INSERT INTO $dbname.Players 
		(name, credits, lifetimeSpins, salt, hashedPassword) 
		VALUES ('testPlayer', $startingCredits, 0, $testSalt, '$testHash')";
	
	try
	{
		$mysql->query($sql);
	}
	catch(PDOException $e)
	{
		error("Error creating test player: " . $e->getMessage(), 500);
	}

	echo(".. Player testPlayer created successfully.<br>");

	echo("Done.")
?>
