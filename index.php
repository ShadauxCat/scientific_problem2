<?php
	require_once('settings.php');

	// Output type is going to be json, error or not.
	header('Content-Type: text/json');

	// Bundle error responses into a json object, set response code, and exit.
	function error($msg, $code)
	{
		echo(json_encode(array("err" => "$msg")));
		http_response_code($code);
		die();
	}

	// Being pedantic, since this only performs update operations, only accept PUT method.
	$method = $_SERVER['REQUEST_METHOD'];
	if($method != 'PUT')
	{
		error("Invalid request type. Only PUT requests are supported.", 405);
	}

	// Parse the payload. Expect it to be json.
	$input = json_decode(file_get_contents('php://input'),true);

	if($input === NULL)
	{
		error("Invalid payload, must be JSON data", 400);
	}

	// Verify all the required data is there, and is of the correct type.
	foreach(array('hash' => "string", 'coinsWon' => "integer", 'coinsBet' => "integer", 'playerID' => "integer") as $key => &$expectedType)
	{
		if(!array_key_exists($key, $input))
		{
			error("Invalid request data: Missing key $key", 400);
		}
		$type = gettype($input[$key]);
		if($expectedType != $type)
		{
			error("Invalid request data: type of '$key' must be '$expectedType', not '$type'", 400);
		}
	}

	// Connect to the database
	$mysql_settings = $settings['mysql'];
	$servername = $mysql_settings['host'];
	$dbname = $mysql_settings['databaseName'];
	try
	{
		$mysql = new PDO("mysql:host=$servername;dbname=$dbname", $mysql_settings['user'], $mysql_settings['password'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
	}
	catch(PDOException $e)
	{
		error("Could not connect to mysql database.", 500);
	}
	
	$playerID = $input['playerID'];

	$credits = 0;
	$lifetimeSpins = 0;
	$name = "";
	
	// Spin in a loop trying to do this update.
	// Since the response has to indicate the new data, I'm doing this as a looped transaction,
	// because that can handle the unlikely case where two updates are received concurrently.
	// Doing a SELECT FOR UPDATE ensures that we take a lock on this row so that the data doesn't possibly change
	// between the SELECT and the UPDATE, so we're definitely not blowing away any other changes to this row.

	// Note that if we didn't have to return the new data, we could avoid this loop and the need for a transaction at all
	// by doing the update query as "UPDATE Players SET `lifetimeSpins` = `lifetimeSpins` + 1, `credits` = `credits` + $totalCoinDelta WHERE `playerID` = $playerID"
	// That single query would do the increments atomically with no race condition between the read and the write to require any locks.
	while(true)
	{
		try
		{
			// Start the transaction.
			$mysql->beginTransaction();

			// Read the data so we can verify the hash and also get a baseline for what we're going to return
			$query = "SELECT * FROM Players WHERE playerID = $playerID FOR UPDATE";
			$result = $mysql->query($query);
			if ($result->rowCount() === 0)
			{
				$mysql->rollback();
				error("No such player.", 400);
			}

			$row = $result->fetch();

			// Verify the hash. I wasn't sure what data was supposed to be hashed here, so I added a hashed password field to the database.
			// Also worth noting: Storing the salt like this rather than just storing the hashed password doesn't work in PHP 7 since the salt parameter
			// is no longer accepted by password_hash().

			// Expecting the hash in base64-encoded format.
			$hash = $row['hashedPassword'];
			if($hash != $input['hash'])
			{
				$mysql->rollback();
				// In a production environment, sending the hash back with the invalid hash response would be a dumb and stupid idea.
				// This is done just to make this code possible to test by providing the user with a valid hash to use.
				error("Invalid hash (should be $hash)", 400);
			}

			// Update the totals and then save them back to the database.
			$totalCoinDelta = $input['coinsWon'] - $input['coinsBet'];
			$credits = $row['credits'] + $totalCoinDelta;
			$lifetimeSpins = $row['lifetimeSpins'] + 1;
			$name = $row['name'];

			$query = "UPDATE Players SET 
				`lifetimeSpins` = $lifetimeSpins,
				`credits` = $credits
				WHERE `playerID` = $playerID";

			$mysql->query($query);
			$mysql->commit();
			// If we got here we've successfully made the changes and can break out of the loop.
			break;
		}
		catch(PDOException $e)
		{
			// If this happened, we probably hit a concurrent update and need to try again.
			// Possible issue: If there's a bug in this code that causes an exception to be thrown for another reason,
			// this could infinite loop. Ideally there are no such bugs, but some work could probably be done to detect that case.
			$mysql->rollback();
		}
	}

	// Now that we've successfully updated, we can make our reply.
	// The noteworthy line here is the calculation of the lifetime average return - because the player doesn't start with 0 credits,
	// and because this implementation does not provide any other way of incrementing credits besides winning, we can use that value
	// with the lifetimeSpins value to calculate the average, BUT first we have to offset it by the starting credits value.
	// An implementation that allowed users to buy more coins rather than just winning them would need to store current coins
	// and lifetime coins won as separate values.
	echo(
		json_encode(
			array(
				"playerID" => $input["playerID"],
				"name" => $name,
				"credits" => $credits,
				"lifetimeSpins" => $lifetimeSpins,
				"lifetimeAverageReturn" => ($credits - $settings['game']['startingCredits'])/$lifetimeSpins
			)
		)
	);

	$mysql->close()
?>
