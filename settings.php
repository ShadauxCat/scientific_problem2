<?php

$settings = array(
	'mysql' => array(
		'host' => 'localhost',
		'user' => 'root',
		'password' => '',
		'databaseName' => 'scientific',
	),
	'game' => array(
		'startingCredits' => 100,
	),
);
?>
